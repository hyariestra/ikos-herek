<?php include 'component/header.php'; ?>
<main id="mainContent" class="main">
  
<header id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopPageBannerImage_banner" class="page-header white-curve" style="background-image:url(../../POPHotels/media/Pop-Hotels/Standard/popbannerlocateus51fb.jpg?ext=.jpg)"></header>

      <div class="locate-us">
        <!-- Article Header-->
        <header class="article__header wingspan">
          

<div class="breadcrumb hidden-xs">
    <div class="wingspan">
        <ul>
            
            <li>
                <span id="p_lt_ctl02_pageplaceholder_p_lt_ctl01_TauziapBreadcrumbs_lblLink">Bali</span>
            </li>
        </ul>
    </div>
</div>
<h1 id="p_lt_ctl02_pageplaceholder_p_lt_ctl01_TauziaPopPageTitle_title">Bali</h1>
        </header>
        <!-- End: Article Header-->
        <!-- Article Body-->
        

<section id="locator">
    <div class="locator__header">
        <div class="locator__filters">
            <div class="destination-filter">
                <div class="selector">
                    <!-- <span class="label">Explore</span> -->
                    <span id="p_lt_ctl02_pageplaceholder_p_lt_ctl02_TauziaPopLocator_lblExploreCaption" class="label">Explore</span>
                    <select class="custom-select" id="destination">
                        <option value="">Destination</option>
                    </select>
        </div>
    </div>
    <div class="hotel-filter">
        <div class="selector">
            <!--<span class="label">and take me to</span>-->
            <span id="p_lt_ctl02_pageplaceholder_p_lt_ctl02_TauziaPopLocator_lblHotelCaption" class="label">and take me to</span>
            <select class="custom-select" id="hotel">
                <option value="">hotel</option>
            </select>
        </div>
    </div>
    </div>
    </div>
    <div class="locator__map"></div>
</section>
<section class="contact-list">
    <div class="wingspan row-col-12">
        <div class="contact-list__container">
            <div class="contact-list__listing"></div>
            <div class="contact-list__loader"><a class="btn btn--style2" href="#"><span>Show More</span></a></div>
        </div>
    </div>
</section>


    <script>
        var mapProps = {dest: 'bali', prop: ''};
    </script>


<script id="contact-template" type="text/template">
    {{~it :value:index}}
        {{? index > 7}}
        <div class="contact-list__contact col-sm-4 col-md-3 hide">
            {{??}}
        <div class="contact-list__contact col-sm-4 col-md-3">
            {{?}}
        	<div class="thumbnail">
                <img src="{{=value.thumbnail}}" alt="" />
            </div>
            <div class="heading">
                <h2>{{=value.hotel_name}}</h2>
            </div>
            <div class="contact">
                <p>{{=value.address}}</p>
                <p>Tel: {{=value.phone}}</p>
                <p>Fax: {{=value.fax}}</p>
                <p><a href="mailto:{{=value.email}}">{{=value.email}}</a></p>
                <!--<ul class = "amenities">
        			{{~value.amenities :amenities:idx}}
        				<li data-toggle="tooltip" data-placement="top" title="{{=amenities.title}}">
        					<img src = '{{=amenities.icon}}' alt = "{{=amenities.title}}">
        				</li>
        			{{~}}
        		</ul>-->
<a href = "{{=value.hotelpage}}" class = "btn btn--style1"><span>View Hotel</span></a>
        	</div>
        </div>
        </div>
        {{~}}
      </script>
        
        <!-- End: Article Body-->
      </div>
 

</main>

<section class="brands">
    <div class="wingspan">
        <div class="brand__parent">
            <a href="../../index.html" id="p_lt_ctl03_TauziapHomepageBrand_litLink"><h2 id="p_lt_ctl03_TauziapHomepageBrand_litTitle" style="background-image:url(../../POPHotels/media/Pop-Hotels/Brands/TAUZIA-logo-03b09f.png?ext=.png);"></h2></a>
        </div>
        <div class="brands__children">
            
            <hr>
            
                    <ul>
                
                    <li>
                        <a id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl01_lnkLink" href="https://preferencehotels.com/" target="_blank"><img id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl01_imgLogo" src="../../../tauzia.s3.amazonaws.com/Tauziav/media/Content/brands/preferenceb09f.png?ext=.png" alt="" /></a>
                    </li>
                
                    <li>
                        <a id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl02_lnkLink" href="https://www.vertuhotels.com/en-US/" target="_blank"><img id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl02_imgLogo" src="../../../tauzia.s3.ap-southeast-1.amazonaws.com/pophotels/media/pop-content/grey_vertub09f.png?ext=.png" alt="" /></a>
                    </li>
                
                    <li>
                        <a id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl03_lnkLink" href="https://www.harrishotels.com/" target="_blank"><img id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl03_imgLogo" src="../../../tauzia.s3.amazonaws.com/Tauziav/media/Content/brands/harrisb09f.png?ext=.png" alt="" /></a>
                    </li>
                
                    <li>
                        <a id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl04_lnkLink" href="https://www.foxhotels.com/en-US/" target="_blank"><img id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl04_imgLogo" src="../../../www.tauziahotels.com/getmedia/8cfb2ebb-fc3b-45e3-b3ca-65490e3cec17/fox_logo_monochromee7a3.png?width=120&amp;height=65&amp;ext=.png" alt="" /></a>
                    </li>
                
                    <li>
                        <a id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl05_lnkLink" href="https://www.yellohotels.com/" target="_blank"><img id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl05_imgLogo" src="../../../tauzia.s3.amazonaws.com/Tauziav/media/Content/brands/yellob09f.png?ext=.png" alt="" /></a>
                    </li>
                
                    <li>
                        <a id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl06_lnkLink" href="../../index.html" target="_blank"><img id="p_lt_ctl03_TauziapHomepageBrand_rptBrands_ctl06_imgLogo" src="../../../tauzia.s3.ap-southeast-1.amazonaws.com/pophotels/media/pop-content/pop-grey-footerb09f.png?ext=.png" alt="" /></a>
                    </li>
                
                    </ul>
                
        </div>
    </div>
</section>

<footer class="site-footer">
  

<div class="back-to-top"><a class="back-top-btn" href="#mainContent" title="Back to top"><i class="icon-arrow_upward"></i></a></div>
<div class="wingspan">
    <div class="row-col-12">
        <div class="site-footer__navigation col-md-5">
            <div class="footer-links__col">
                

<h3>
    
</h3>

        <ul>
    
        <li>
            <a id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl01_lnkCTA" href="../About-Us.html"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl01_lblCTA">About Us</span></a></li>
    
        <li>
            <a id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl02_lnkCTA" href="../Facilities.html"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl02_lblCTA">Facilities</span></a></li>
    
        <li>
            <a id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl03_lnkCTA" href="../Discover.html"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl03_lblCTA">Discover</span></a></li>
    
        </ul>
    

            </div>
            <div class="footer-links__col">
                

<h3>
    
</h3>

        <ul>
    
        <li>
            <a id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl01_lnkCTA" href="../Terms-Conditions.html"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl01_lblCTA">Terms & Conditions</span></a></li>
    
        <li>
            <a id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl02_lnkCTA" href="../Privacy-Policy.html"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl02_lblCTA">Privacy Policy</span></a></li>
    
        <li>
            <a id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl03_lnkCTA" href="../Customer-Relations.html"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl03_lblCTA">Customer Relations</span></a></li>
    
        </ul>
    

            </div>
        </div>
        <div class="site-footer__social-links col-md-3">
            <p>Connect with us</p>
           

<ul>
    
    <li><a href="https://www.instagram.com/pophotels/?hl=en"><i class="icon-instagram"></i><span class="sr-only">Instagram</span></a></li>
    
    <li><a href="https://www.facebook.com/POPhotels/"><i class="icon-facebook"></i><span class="sr-only">Facebook</span></a></li>
    
    <li><a href="https://www.youtube.com/user/TAUZIAchannel"><i class="icon-youtube"></i><span class="sr-only">Youtube</span></a></li>
    
</ul>

        </div>
        <div class="site-footer__subscription col-md-4">
            <p>Newsletter</p>
             <form style="display: none"></form>
            <form class="subscription-form" method="post" accept-charset="UTF-8" action="https://eepurl.com/gv1IpX" target="_blank">
                <label class="sr-only" for="subscription">Email address</label>
                <input id="subscription" type="text" placeholder="Email address">
                <button class="btn btn--style1" type="submit">Sign Up</button>
            </form>
        </div>
    </div>
</div>
<div class="site-footer__copyright"><small id="p_lt_ctl04_TauziaPopSiteFooter_lblCopyright">&#169; Copyright 2020 TAUZIA Hotels, All rights reserved.</small></div>

</footer>
 <script src="../../assets/pophotels/js/vendor/jquery-3.1.1.min.js"></script>
<script src="../../assets/pophotels/js/main.js"></script>
    
    

<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {$get("manScript_HiddenField").value = '';Sys.Application.remove_init(fn);};Sys.Application.add_init(fn);})();
var callBackFrameUrl='../../WebResource6049.html?d=beToSAE3vdsL1QUQUxjWdSeUJlULefCEMVrN7YTAtqv-o1lpJOSRgH3PwKoC_VlMrmgEcYX5_mhljJ1CiwJ3qg2&amp;t=636765715264470882';
WebForm_InitCallback();
theForm.oldSubmit = theForm.submit;
theForm.submit = WebForm_SaveScrollPositionSubmit;

theForm.oldOnSubmit = theForm.onsubmit;
theForm.onsubmit = WebForm_SaveScrollPositionOnSubmit;
//]]>
</script>
</form>
</body>

<!-- Mirrored from www.pophotels.com/en-US/Pick-a-POP!/Bali by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Feb 2020 06:56:05 GMT -->
</html>
