<?php include 'component/header.php' ?>


<main id="mainContent" class="main">
  

<section class="standard-page">
        <div class="wingspan">
          <!-- Standard article wrapper-->
          <article class="article standard-article">
            <!-- Article Header-->
            <header class="article__header">
             


<h1 id="p_lt_ctl02_pageplaceholder_p_lt_ctl01_TauziaPopPageTitle_title">About Us</h1>




            </header>
            <!-- End: Article Header-->
            <!-- Article Body-->
            <section class="article__body">
               <div style="text-align: center;"><strong>KOS EXCLUSIVE JOGJA</strong></div>
<br />
  <p>iKos menawarkan hunian sementara yang nyaman, bersih dan exclusive dalam bentuk kamar sewa (kost exclusive) yang dapat disewa harian, mingguan dan bulanan. Dengan fasilitas lengkap seperti kamar Full AC, spring bed, kamar mandi dalam, water heater, TV cable, WiFi, parkir motor dan mobil, security 24 jam, CCTV, free cleaning service menghadirkan kenyamanan dan suasana seperti layaknya Anda berada di hotel berbintang.</p> 

<p>Kami berusaha memberikan produk layanan dan fasilitas hunian yang lebih mencerminkan gaya hidup modern kekinian. Bentuk bangunan yang modern dan dirancang khusus untuk kenyamanan tempat tinggal, desain interior dan eksterior yang kekinian, penjaga yang ramah dan cekatan menjadikan hunian kami unik dan berbeda dengan kost exclusive lainnya.</p>

<p>Kami sangat memahami siapa pelanggan kami, berdasarkan pengalaman dan apa saja yang mereka butuhkan. Para mahasiswa, karyawan, pasangan muda dan penyewa yang membutuhkan privasi tinggi. Segmen pelanggan ini memiliki latar belakang sosial yang baik dan perawatan aset harian sehingga memudahkan kami dalam mengelola hunian.</p> 

<p>Nilai-nilai kemudahan, kenyamanan, eksklusivitas, privasi dan modern inilah yang menjadikan hunian di iKos mampu menjadi brand image utama dalam pemilihan kost exclusive di kota Yogyakarta. Saat ini kami memiliki 7 kost exclusive yang tersebar di Yogyakarta yakni Seturan, Condong catur, Palagan, Monjali, serta Jakal (Comming Soon)</p> 

<br>



           </section>
          </article>
          <!-- End: Standard article wrapper-->
        </div>
      </section>

</main>



<?php include 'component/footer.php' ?>