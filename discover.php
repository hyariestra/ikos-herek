<?php include 'component/header.php'; ?>

<style>

    .ikon{
        width: 20px;
        margin-right: 10px;

    }
    .paragraphs{
        padding: 20px;
    }
    .card {
  /*  position:absolute;
    top:50%;
    left:50%;*/
    /*   transform:translate(-50%,-50%);*/
    width:400px;
    min-height:400px;
    background:#fff;
    box-shadow:0 20px 50px rgba(0,0,0,.1);
    border-radius:5px;
    transition:0.5s;
}
.card:hover {
    box-shadow:0 30px 70px rgba(0,0,0,.2);
}
.card-box{
    padding: 20px;
}


</style>

<?php
include 'koneksi.php';
$url = $_GET['url'];
$data = mysqli_query($koneksi,"SELECT * FROM kosan WHERE url = '$url' ");

while($d = mysqli_fetch_array($data)){

  $gambar = mysqli_query($koneksi,"SELECT * FROM gambar WHERE id_kamar = ".$d['id']." ");
  $c = mysqli_query($koneksi,"SELECT * FROM gambar WHERE id_kamar = ".$d['id']." ");

  ?>

  <main id="mainContent" class="main">


    <div class="locate-us">
        <!-- Article Header-->
        <header class="article__header wingspan">



            <h1 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl01_TauziaPopPageTitle_title"><?php echo $d['judul']; ?></h1>
        </header>
        <!-- End: Article Header-->
        <!-- Article Body-->
        

        <section class="contact-list">
            <div class="wingspan row-col-12">
                <div class="contact-list__container">
                    <div class="contact-list__listing">   

                        <div id="carousel-id" class="carousel slide" data-interval="false" data-ride="carousel">

                            <ol class="carousel-indicators">

                                <?php
                                $x = 0;
                                while($e = mysqli_fetch_array($c)){
                                    ?>
                                    <?php  $actt =  $x == 0 ?'active':'';   ?>
                                    <li data-target="#carousel-id" data-slide-to="<?php echo $x ?>" class="<?php echo $actt ?>"></li>
                                    <?php 
                                    $x++;
                                }
                                ?>

                            </ol>
                            <div class="carousel-inner">



                              <?php
                              $no = 1;
                              while($e = mysqli_fetch_array($gambar)){
                                ?>
                                <?php  $act =  $no == 1 ?'active':'';   ?>

                                <div class="item <?php echo $act ?>  ">
                                    <img height="300px" width="1300px" data-src="holder.js/900x500/auto/#555:#5a5a5a/text:Third slide" alt="Third slide" src="image/<?php echo $e['file']; ?>">

                                </div>

                                <?php 
                                $no++;
                            }
                            ?>




                        </div>
                        <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                        <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </div>
                    <?php if ($d['judul_bawah']!=""): ?>
                        <header class="article__header wingspan">
                            <h1 id="p_lt_ctl02_pageplaceholder_p_lt_ctl01_TauziaPopPageTitle_title"> <?php echo $d['judul_bawah'] ?></h1>
                        </header>
                    <?php endif ?>
                    <div class="col-lg-8 paragraphs">
                        <p><b>Harga</b></p>
                        <img style="float:left" class="ikon" src="image/price.png"/>
                        <div class="content-heading">
                            <span><?php echo $d['harga']; ?></span> 
                        </div>

                        <br>
                        <?php if ($d['terima']!=""): ?>
                            <p><b>Terima</b></p>

                            <div class="row">
                             <div class="col-lg-4">

                                 <div class="content-heading">
                                    <span><?=  $d['terima'] ?></span> 
                                </div>
                            </div>

                        </div>
                    <?php endif ?>
                    <br>



                    <div class="fasilitas">


                        <div class="row">
                           <div class="col-lg-12">
                            <p><b>Fasilitas kost dan kamar :</b></p>
                        </div> 


                        <?php if ($d['ac']!=""): ?>
                            <div class="col-lg-4">
                               <img style="float:left" class="ikon" src="image/ac.png"/>
                               <div class="content-heading">
                                <span><?php echo $d['ac']; ?> </span> 
                            </div>
                        </div>                    
                    <?php endif ?>


                    <?php if ($d['tv']!=""): ?>
                        <div class="col-lg-4">
                           <img style="float:left" class="ikon" src="image/tv.png"/>
                           <div class="content-heading">
                            <span><?php echo $d['tv']; ?> </span> 
                        </div>
                    </div>                    
                <?php endif ?>

                <?php if ($d['kasur']!=""): ?>
                    <div class="col-lg-4">
                      <img style="float:left" class="ikon" src="image/kasur.png"/>
                      <div class="content-heading">
                        <span><?php echo $d['kasur']; ?></span> 
                    </div>
                </div>
            <?php endif ?>


            <?php if ($d['lemari']!=""): ?>
                <div class="col-lg-4">
                   <img style="float:left" class="ikon" src="image/lemari.png"/>
                   <div class="content-heading">
                    <span><?php echo $d['lemari']; ?> </span> 
                </div>
            </div>                    
        <?php endif ?>


        <?php if ($d['meja']!=""): ?>
            <div class="col-lg-4">
             <img style="float:left" class="ikon" src="image/desk.png"/>
             <div class="content-heading">
                <span><?php echo $d['meja']; ?> </span> 
            </div>
        </div>                    
    <?php endif ?>

    <?php if ($d['wifi']!=""): ?>
        <div class="col-lg-4">
            <img style="float:left" class="ikon" src="image/wifi.png"/>
            <div class="content-heading">
                <span><?php echo $d['wifi']; ?></span> 
            </div>
        </div>
    <?php endif ?>


    <?php if ($d['kamarmandi']!=""): ?>

        <div class="col-lg-4">
          <img style="float:left" class="ikon" src="image/km.png"/>
          <div class="content-heading">
            <span><?php echo $d['kamarmandi']; ?></span> 
        </div>
    </div>
<?php endif ?>


<?php if ($d['heater']!=""): ?>
    <div class="col-lg-4">
     <img style="float:left" class="ikon" src="image/heater.png"/>
     <div class="content-heading">
        <span><?php echo $d['heater']; ?> </span> 
    </div>
</div>                    
<?php endif ?>


<?php if ($d['shower']!=""): ?>
    <div class="col-lg-4">
     <img style="float:left" class="ikon" src="image/shower.png"/>
     <div class="content-heading">
        <span><?php echo $d['shower']; ?> </span> 
    </div>
</div>                    
<?php endif ?>


<?php if ($d['wc']!=""): ?>
    <div class="col-lg-4">
        <img style="float:left" class="ikon" src="image/wc.png"/>
        <div class="content-heading">
            <span><?php echo $d['wc']; ?></span> 
        </div>
    </div>
<?php endif ?>

<?php if ($d['wastafel']!=""): ?>
    <div class="col-lg-4">
       <img style="float:left" class="ikon" src="image/wastafel.png"/>
       <div class="content-heading">
        <span><?php echo $d['wastafel']; ?> </span> 
    </div>
</div>                    
<?php endif ?>

<?php if ($d['akses']!=""): ?>
    <div class="col-lg-4">
     <img style="float:left" class="ikon" src="image/akses.png"/>
     <div class="content-heading">
        <span><?php echo $d['akses']; ?> </span> 
    </div>
</div>
<?php endif ?>


<div style="margin-top: 20px;" class="col-lg-12">
    <p><b>Fasilitas Umum :</b></p>
</div>

<?php if ($d['dapur']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/dapur.png"/>
      <div class="content-heading">
        <span><?php echo $d['dapur']; ?></span> 
    </div>
</div>
<?php endif ?>

<?php if ($d['kulkas']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/kulkas.png"/>
      <div class="content-heading">
        <span><?php echo $d['kulkas']; ?></span> 
    </div>
</div>
<?php endif ?>

<?php if ($d['dispenser']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/dispenser.png"/>
      <div class="content-heading">
        <span><?php echo $d['dispenser']; ?></span> 
    </div>
</div>
<?php endif ?>


<?php if ($d['ruangtamu']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/ruangtamu.png"/>
      <div class="content-heading">
        <span><?php echo $d['ruangtamu']; ?></span> 
    </div>
</div>
<?php endif ?>

<?php if ($d['gazebo']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/gazebo.png"/>
      <div class="content-heading">
        <span><?php echo $d['gazebo']; ?></span> 
    </div>
</div>
<?php endif ?>


<?php if ($d['jemur']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/jemur.png"/>
      <div class="content-heading">
        <span><?php echo $d['jemur']; ?></span> 
    </div>
</div>
<?php endif ?>


<?php if ($d['parkir']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/parkir.png"/>
      <div class="content-heading">
        <span><?php echo $d['parkir']; ?></span> 
    </div>
</div>
<?php endif ?>

<?php if ($d['cctv']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/cctv.png"/>
      <div class="content-heading">
        <span><?php echo $d['cctv']; ?></span> 
    </div>
</div>
<?php endif ?>

<?php if ($d['security']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/security.png"/>
      <div class="content-heading">
        <span><?php echo $d['security']; ?></span> 
    </div>
</div>
<?php endif ?>

<?php if ($d['pool']!=""): ?>

    <div class="col-lg-4">
      <img style="float:left" class="ikon" src="image/pool.png"/>
      <div class="content-heading">
        <span><?php echo $d['pool']; ?></span> 
    </div>
</div>
<?php endif ?>



</div>



</div>
<br>
<div  class="deskripsi">
    <p><b>Deskripsi Kamar</b></p>
    <?php echo $d['deskripsi']; ?>
</div>
<hr>
<div class="maps">
    <?php echo $d['maps']; ?> 

    <!--   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.283573304521!2d110.40669181397476!3d-7.759720579102892!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a599bd3bdc4ef%3A0x6f1714b0c4544586!2sUniversitas%20Amikom%20Yogyakarta!5e0!3m2!1sid!2sid!4v1582909643844!5m2!1sid!2sid" width="780" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe> -->



</div>


</div>
<div style="margin-top: 10px" class="col-lg-4">
 <div class="card">
    <div class="card-box">
        <p><b>Keterangan Tambahan</b></p>
        <div class="row">

            <?php if ($d['luas']!=""): ?>
                <div class="col-lg-12">
                    <img style="float:left" class="ikon" src="image/brick.png"/>
                    <div class="content-heading">
                        <span><?php echo $d['luas']; ?></span> 
                    </div>
                </div>
            <?php endif ?>
            <br>
            <br>
            <div class="col-lg-12">
                <img style="float:left" class="ikon" src="image/plug.png"/>
                <div class="content-heading">
                    <span><?php echo $d['listrik']; ?></span> 
                </div>
            </div>
            <br>
            <br>
            <div class="col-lg-12">
                <img style="float:left" class="ikon" src="image/pay.png"/>
                <div class="content-heading">
                    <span><?php echo $d['min_bayar']; ?></span> 
                </div>
            </div>


        </div>

    </div>
</div>

</div>
</div>
</div>
</section>


<?php 
}
?>


<!-- End: Article Body-->
</div>


</main>



<?php include 'component/footer.php' ?>