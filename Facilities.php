<?php include 'component/header.php' ?>

<main id="mainContent" class="main">
  <!-- 
    <header id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopPageBannerImage_banner" class="page-header" style="background-image:url(../../tauzia.s3.ap-southeast-1.amazonaws.com/pophotels/media/pop-hotels/standard/facilitiesbannerpopnew51fb.jpg?ext=.jpg)"></header> -->

    <section class="standard-page">
        <div class="wingspan">
          <!-- Standard article wrapper-->
          <article class="article standard-article">
            <!-- Article Header-->
            <header class="article__header">

                <h1 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl01_TauziaPopPageTitle_title">Fasilitas</h1>

            </header>
            <!-- End: Article Header-->
            <!-- Article Body-->
            <section class="article__body">
             <strong>ROOM</strong><br />
             &nbsp;<br />
             A smart compact layout of 15 sqm room size, equipped with king size bed and a comfy &ldquo;Dream Care&rdquo; mattress, exclusively developed by King Koil for POP! Hotels.<br />
             100% cotton linen will even give you a better &ldquo;good night&rdquo; sleep.<br />
             Completed with comfortable 3<sup>rd</sup> bed with extra blanket that will allow to have 3 persons in a room, safe deposit box with laptop size, bath towels and simple amenities.<br />
             &nbsp;

             <div class="side-by-side">
                <div class="col">
                    <div class="img-holder">
                        <img src="image/1.jpg" id="p_lt_ctl02_pageplaceholder_p_lt_ctl02_TauziaPopSideBySideImage_imageLeft" alt="" />
                    </div>
                </div>
                <div class="col">
                    <div class="img-holder">
                        <img src="image/2.jpg" id="p_lt_ctl02_pageplaceholder_p_lt_ctl02_TauziaPopSideBySideImage_imageRight" alt="" />
                    </div>
                </div>
            </div>
            <strong>PITSTOP</strong><br />
            The integration of the Café + convenience store into the lobby area creates an animated and interactive public social hub.<br />
            With a lively lounge atmosphere, everyone is invited to converge, work, meet, play or simply enjoy a cup of fresh coffee or a light meal while using free high-speed Wifi connection.<br />
            &nbsp;

            <div class="side-by-side">
                <div class="col">
                    <div class="img-holder">
                        <img src="image/3.jpg" id="p_lt_ctl02_pageplaceholder_p_lt_ctl02_TauziaPopSideBySideImage1_imageLeft" alt="" />
                    </div>
                </div>
                <div class="col">
                    <div class="img-holder">
                        <img src="image/5.jpg" id="p_lt_ctl02_pageplaceholder_p_lt_ctl02_TauziaPopSideBySideImage1_imageRight" alt="" />
                    </div>
                </div>
            </div>
            <p><b>Meeting Room</b></p>

            <p>Ready to succeed your business, POP! Hotels provided meeting rooms, equipped with meeting facilities: flipchart, projector, meeting notes with full access to Wifi connection.</p>


            <div class="side-by-side">
                <div class="col">
                    <div class="img-holder">
                        <img src="image/4.jpg" id="p_lt_ctl02_pageplaceholder_p_lt_ctl02_TauziaPopSideBySideImage2_imageLeft" alt="" />
                    </div>
                </div>
                <div class="col">
                    <div class="img-holder">
                        <img src="image/5.jpg" id="p_lt_ctl02_pageplaceholder_p_lt_ctl02_TauziaPopSideBySideImage2_imageRight" alt="" />
                    </div>
                </div>
            </div>
            <p><b>Others</b></p>

            <p>Free Wifi, security with 24 hours CCTV, International / local TV channel, Internet corner 24 hours and friendly staff that ready to serve you for 24 hours.</p>

        </section>
    </article>
    <!-- End: Standard article wrapper-->
</div>
</section>

</main>




<?php include 'component/footer.php' ?>