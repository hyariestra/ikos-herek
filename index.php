<?php include 'component/header.php' ?>



<main id="mainContent" class="main">

    

<section class="home-section home-first-section">
    <div class="wingspan">
        <!-- Home Offer-->
        <div class="home-offer row-col-12">
            <article class="home-offer__snippet col-md-4 col-sm-6">
                <h2 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSpecialOffer_offerTitle">iKostel</h2>
                <p class="setFont">Lokasi sangat strategis dekat kampus-kampus terbaik dan ternama di Jogja, selain itu juga dekat dengan mall-mall terbesar di Jogja<br />
                    <br />
                Menjadikan hunian yang ideal buat mahasiswa, karyawan maupun pasangan muda yang cari hunian sementara berupa kamar (kos Excluisve).</p>
                <a href="https://wa.me/62817420777" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSpecialOffer_ctaLink" class="hidden-xs btn btn--style2">Book Now</a>
            </article>
            <div class="home-offer__thumbnail col-md-5 col-sm-6 offset-md-1">
             <!--    <div class="shout-out"><span id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSpecialOffer_shoutOutText"></span></div>
              -->   <div class="img-holder">
                    <img  src="image/banner-top.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSpecialOffer_offerImage" alt="" /></div>
                </div>
                <div class="button-holder">
                    <a href="https://wa.me/62817420777" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSpecialOffer_ctaLinkMobile" class="visible-xs-inline-block btn btn--style2">Book Now</a>
                </div>
            </div>
            <!-- End: Home Offer-->
        </div>
        <img class="pop-pop pop-right-top" src="assets/pophotels/img/home-red-blue-pop.png">
        <img class="pop-pop pop-right-bottom" src="assets/pophotels/img/home-purple-orange-pop.png">
        <img class="pop-pop pop-left-middle" src="assets/pophotels/img/home-red-yellow-pop.png">
    </section>


    <section class="home-mtp">
        <div id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeCTA_imageBackground" class="home-mtp__img" style="background-image:url(image/pop_saverz.png)">
            <img src="image/pop_saverz.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeCTA_image" alt="" /></div>
            <div class="wingspan">
                <div class="row-col-12">
                    <div class="col-md-6 offset-md-6 col-sm-6 offset-sm-6">
                        <div class="home-mtp__text">
                            <h2><span id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeCTA_title" class="sr-only">My Tauzia Privilege</span><img height="300px" src="image/jogja.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeCTA_logo" class="mtp-logo" alt="" /></h2>
                            <p id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeCTA_text">Tinggal dan nikmati hal-hal unik serta tradisional di Jogja</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="home-section">
            <div class="wingspan">
                <!-- Home Locate Us-->
                <div class="home-locate-us">
                    <div class="home-locate-us__desc">
                        <h2 class="setFont"  id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_title">DISCOVER</h2>

                    </div>
                    <div class="home-locate-us__destination">

                        <div id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_dest1" class="destination-item destination-item--style1">
                            <a href="discover.php?url=iKos-Amory-Seturan" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destLink1">
                                <div class="img-holder">
                                    <img  src="image/circle/seturan_circle.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destImage1" alt="" /><span id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destMore1" class="label">VIEW MORE</span>
                                </div>
                                <h4 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destName1" class="location-name">SETURAN</h4>
                            </a>
                        </div>

                        <div id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_dest2" class="destination-item destination-item--style2">
                            <a href="discover.php?url=iKos-Nusa-Indah" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destLink2">
                                <div class="img-holder">
                                    <img src="image/circle/nusa_indah_circle.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destImage1" alt="" /><span id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destMore1" class="label">VIEW MORE</span>
                                </div>
                                <h4 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destName2" class="location-name">NUSA INDAH</h4>
                            </a>
                        </div>

                        <div id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_dest3" class="destination-item destination-item--style3">
                            <a href="discover.php?url=iKos-Villa-Green-Palagan" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destLink3">
                                <div class="img-holder">
                                     <img  src="image/circle/palagan_circle.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destImage3" alt="" /><span id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destMore3" class="label">VIEW MORE</span>
                                </div>
                                <h4 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destName3" class="location-name">PALAGAN</h4>
                            </a>
                        </div>

                        <div id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_dest4" class="destination-item destination-item--style4">
                            <a href="discover.php?url=iKos-Monjali" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destLink4">
                                <div class="img-holder">
                                    <img  src="image/circle/monjali_circle.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destImage4" alt="" /><span id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destMore4" class="label">VIEW MORE</span>
                                </div>
                                <h4 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeProperty_destName4" class="location-name">Monjali</h4>
                            </a>
                        </div>

                    </div>
                </div>
                <!-- End: Home Locate Us-->
            </div>
        </section>


        <!-- Home Values-->
        <section class="home-values">
            <div class="wingspan">
                <div style="padding: 15px" class="row-col-12">
                  <div style="padding: 20px" align="center" class="col-lg-12">
                     <p class="value_head">Kenapa iKos?</p>
                  </div>

                   <div align="center" class="col-lg-4">
                     <img src="image/location.png" alt="">
                     <p class="value">Lokasi strategis</p>
                 </div>
                 <div align="center" class="col-lg-4">
                  <img src="image/lengkap.png" alt="">
                  <p class="value">Fasilitas lengkap</p>
              </div>
              <div align="center"  class="col-lg-4">
               <img src="image/clean.png" alt="">
               <p class="value">bersih & terawat</p>
           </div>
       </div>
   </div>


</section>
<!-- End: Home Values-->



<!-- End: Home Second Last Section-->


<section class="home-section home-last-section">
    <div class="wingspan">
        <!-- Home Social-->
        <div class="home-social">
            <div class="home-social__desc">
              <!--   <p id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_subTitle">@POPHotels</p> -->
              <div class="home-social__links">


                <ul>

                    <li><a href="https://www.instagram.com/ikostel.id/"><i class="icon-instagram"></i><span class="sr-only">Instagram</span></a></li>

                    <li><a href="#"><i class="icon-facebook"></i><span class="sr-only">Facebook</span></a></li>

                    <li><a href="#"><i class="icon-youtube"></i><span class="sr-only">Youtube</span></a></li>

                </ul>

            </div>

        </div>

        <div class="home-social__features row-col-12">

            <div id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_feature1" class="col-sm-4 col-md-4 feature-item feature-item--style1">


                <div class="img-holder">
                    <a href="https://www.instagram.com/ikostel.id/" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureLink1">
                        <img  src="image/kolam.JPG" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureImage1" alt="" /><span class="label label--style1"><i id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureIcon1" class="icon-instagram"></i></span>
                    </a>
                </div>
                <h3 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureTitle1">Renang sepuasnya di iKos Villa Green Palagan</h3>

                
            </div>

            <div id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_feature2" class="col-sm-4 col-md-4 feature-item feature-item--style2">


                <div class="img-holder">
                    <a href="https://www.instagram.com/ikostel.id/" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureLink2">
                        <img  src="image/fresh.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureImage2" alt="" /><span class="label label--style2"><i id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureIcon2" class="icon-instagram"></i></span>
                    </a>
                </div>
                <h3 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureTitle2">Bangunan Baru Fresh</h3>


            </div>

            <div id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_feature3" class="col-sm-4 col-md-4 feature-item feature-item--style3">


                <div class="img-holder">
                    <a href="https://www.instagram.com/ikostel.id/" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureLink3">
                        <img  src="image/lokasi.png" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureImage3" alt="" /><span class="label label--style3"><i id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureIcon3" class="icon-instagram"></i></span>
                    </a>
                </div>
                <h3 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeSocial_featureTitle3">Lokasi dekat kampus YKPN dan UPN Seturan </h3>

                
            </div>

        </div>
    </div>
    <!-- End: Home Social-->
</div>
</section>

</main>



<!-- footer -->

<?php include 'component/footer.php';?>
