        <footer class="site-footer">
          

            <div class="back-to-top"><a class="back-top-btn" href="#mainContent" title="Back to top"><i class="icon-arrow_upward"></i></a></div>
            <div class="wingspan">
                <div class="row-col-12">
                    <div class="site-footer__navigation col-md-5">
                        <div class="footer-links__col">
                            

                            <h3>
                                
                            </h3>

                            <ul>
                                
                                <li>
                                    <a id="#"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl01_lblCTA">About Us</span></a></li>
                                    
                                    <li>
                                        <a id="#"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl02_lblCTA">Facilities</span></a></li>
                                        
                                        <li>
                                            <a id="#"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks1_rptLink_ctl03_lblCTA">Discover</span></a></li>
                                            
                                        </ul>
                                        

                                    </div>
                                    <div class="footer-links__col">
                                        

                                        <h3>
                                            
                                        </h3>

                                        <ul>
                                            
                                            <li>
                                                <a id="#"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl01_lblCTA">Terms & Conditions</span></a></li>
                                                
                                                <li>
                                                    <a id="#"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl02_lblCTA">Privacy Policy</span></a></li>
                                                    
                                                    <li>
                                                        <a id="#"><span id="p_lt_ctl04_TauziaPopSiteFooter_FooterLinks2_rptLink_ctl03_lblCTA">Customer Relations</span></a></li>
                                                        
                                                    </ul>
                                                    

                                                </div>
                                            </div>
                                            <div class="site-footer__social-links col-md-3">
                                                <p>Connect with us</p>
                                                

                                                <ul>
                                                    
                                                    <li><a href="https://www.instagram.com/ikostel.id/"><i class="icon-instagram"></i><span class="sr-only">Instagram</span></a></li>
                                                    
                                                    <li><a href="#"><i class="icon-facebook"></i><span class="sr-only">Facebook</span></a></li>
                                                    
                                                    <li><a href="#"><i class="icon-youtube"></i><span class="sr-only">Youtube</span></a></li>
                                                    
                                                </ul>

                                            </div>
                                            <div class="site-footer__subscription col-md-4">
                                                <p>Newsletter</p>
                                                <form style="display: none"></form>
                                                <form class="subscription-form" method="post" accept-charset="UTF-8" action="https://eepurl.com/gv1IpX" target="_blank">
                                                    <label class="sr-only" for="subscription">Email address</label>
                                                    <input id="subscription" type="text" placeholder="Email address">
                                                    <button class="btn btn--style1" type="submit">Sign Up</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="site-footer__copyright"><small id="p_lt_ctl04_TauziaPopSiteFooter_lblCopyright">&#169; Copyright <?php echo date("Y"); ?> iKostel, All rights reserved.</small></div>

                                </footer>
                                <script src="assets/pophotels/js/vendor/jquery-3.1.1.min.js"></script>
                                <script src="assets/pophotels/js/main.js"></script>
                                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
                                <script src="assets/pophotels/slick/slick/slick.min.js"></script>
                                

                                    <script>

                                    $('.slider-for').slick({
                                      slidesToShow: 1,
                                      slidesToScroll: 1,
                                      arrows: false,
                                      fade: true,
                                      asNavFor: '.slider-nav'
                                  });
                                    $('.slider-nav').slick({
                                      slidesToShow: 3,
                                      slidesToScroll: 1,
                                      asNavFor: '.slider-for',
                                      dots: true,
                                      centerMode: true,
                                      focusOnSelect: true
                                  });



//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
} else {
    mybutton.style.display = "none";
}
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>



</body>

<!-- Mirrored from www.pophotels.com/en-US/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Feb 2020 06:55:53 GMT -->
</html>
