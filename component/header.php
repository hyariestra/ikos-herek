<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" class="no-js">

<!-- Mirrored from www.pophotels.com/en-US/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Feb 2020 06:55:12 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head id="head"><title>
  iKos - Kos dan homestay Nyaman, murah  di Yogyakarta
</title><meta name="description" content="Kos dan homestay termurah di Yogyakarta yang sangat cocok untuk wisatawan, backpaker, perjalanan,pelahar bisnis dan perjalanan di akhir pekan. Nikmati hotel murah di berbagai destinasi Populer dan tempat strategis di Yogyakarta seperti Seturan, kaliurang, Monjali, Palagan, Banjarmasin, dan lainnya. Pesan sekarang!
" /> 
<meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
<meta http-equiv="pragma" content="no-cache" /> 
<meta http-equiv="content-style-type" content="text/css" /> 
<meta http-equiv="content-script-type" content="text/javascript" /> 
<meta name="keywords" content="Hotel murah, Hotel Termuah, homestay murah jogja" /> 
<link rel="icon" href="pop%21.ico">
<link rel="shortcut icon" href="pop%21.ico">

<!-- PushAlert -->

<!-- End PushAlert --> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Kos Nyaman, murah untuk putra putri  di Yogjakarta
">
<meta name="keywords" content="kos murah, kos Termuah, kos jogja">
<meta property="og:image" content=""/>
<meta property="og:title" content=""/>
<meta property="og:description" content=""/>
<meta property="og:url" content="http://ikostel.com"/>
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="ikostel"/>
<meta name="twitter:card" content=""/>
<meta name="twitter:title" content=""/>
<meta name="twitter:description" content=""/>
<meta name="twitter:image" content=""/>

<link rel="alternate" href="index-2.html" hreflang="en" />
<link rel="apple-touch-icon" href="assets/pophotels/img/apple-touch-icon.png">
<link rel="stylesheet" type="text/css" href="cloud.typography.com/7054514/6550152/css/fonts.css">
<link rel="stylesheet" href="assets/pophotels/css/main.css">

<script src="assets/pophotels/js/vendor/modernizr.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body class="LTR ENUS ContentBody" >


  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TVPL88T"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) --> 

    <style>
      .centered {
        position: absolute;
        top: 85%;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 1% 20% 1% 20%;
        background: rgba(98, 159, 211, 0.5); 
      }
      .con {
        position: relative;
        text-align: center;
        color: white;
        font-size: 50px;
      } 

      p.title_slide{
        color: #629fd3;
        font-family: 'VAGRoundedLTPro-Black';
      }

      .setFont{
           font-family: 'VAGRoundedLTPro-Black';
      }

      .site-logo{
        height: 80px !important;
        width: 100px !important;
      }



      #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
      }
      p.value{
       color: #fff;
       font-family: 'VAGRoundedLTPro-Black';
       font-weight: normal;
       font-style: normal;
       font-size: 20px;

     }

     p.value_head{
       color: #fff;
       font-family: 'VAGRoundedLTPro-Black';
       font-weight: normal;
       font-style: normal;
       font-size: 30px;

     }
   </style>

   <header class="site-header">
    <div class="site-header__main">

      <div class="wingspan">
        <a class="site-logo hidden-xs hidden-sm" href="index.php"><span class="sr-only">Ikos</span></a>
        <a class="site-logo-mobile hidden-md hidden-lg" href="index.php"><span class="sr-only">Ikos</span></a>
        <div class="mobile-triggers visible-xs-block visible-sm-block">
          <a class="mobile-menu-trigger" href="#">
            <span class="label">
              <i class="menu-line"></i>
              <i class="menu-line"></i>
              <i class="menu-line"></i>
            </span>
            <span class="close">
              <i class="menu-line"></i>
              <i class="menu-line"></i>
              <i class="menu-line"></i>
            </span>
          </a>

        </div>


        <div class="navigator">
          <nav class="site-navigation">

            <ul>

              <li>
                <a id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl01_lnkLink" href="About-Us.php"><span id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl01_lblLink">Tentang Kami</span></a>

              </li>

              <li>
                <a id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl02_lnkLink" href="#"><span id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl02_lblLink">kos Exclusive</span></a>

                <ul class="site-navigation__children">
                 <?php
                 include 'koneksi.php';
                 $data = mysqli_query($koneksi,"SELECT * FROM kosan");

                 while($d = mysqli_fetch_array($data)){

                  ?>

                  <li>
                    <a id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl02_rptNavigation_ctl01_lnkLink" href="discover.php?url=<?php echo $d['url'] ?>  "><span id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl02_rptNavigation_ctl01_lblLink"><?php echo $d['judul'] ?></span></a>
                  </li>

                  <?php 
                }
                ?>
              </ul>

            </li>

            <li>
              <a id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl03_lnkLink" href="Facilities.php"><span id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl03_lblLink">Fasilitas</span></a>

            </li>

            <li>
              <a id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl04_lnkLink" href="explore.php"><span id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl04_lblLink">Destinasi</span></a>

            </li>

            <li>
              <a id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl05_lnkLink" href="Gallery.php"><span id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl05_lblLink">Galeri</span></a>

            </li>

            <li>
              <a id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl06_lnkLink" href="maps.php"><span id="p_lt_ctl00_TauziaPopSiteHeader_rptNavigation_ctl06_lblLink">Maps</span></a>

            </li>

          </ul>


                    <!-- <span class="bookmask-trigger">
                        <a class="btn btn--style2" href="maps.php">
                            <span id="p_lt_ctl00_TauziaPopSiteHeader_lblBookNow">Maps</span><i class="icon-angle-down"></i>
                          </a></span> -->

                        </nav>



                      </div>
                    </div>
                  </div>




                </header>
                <header id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeHeroBanner_bannerImage" class="home-banner" style="background-image:url(image/banner.jpeg)">
                  <div class="wingspan">
                    <div class="row-col-12 hidden-xs">
                      <div class="home-banner__text col-md-4 offset-md-8">
                        <h2 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl00_TauziaPopHomeHeroBanner_bannerText">Kos kosan dan homestay dengan pengalaman berbeda! </h2>
                      </div>
                    </div>
                  </div>
                </header>
                <a target="_blank" href="https://wa.me/62817420777">
                  <img  id="myBtn" width="85px" src="image/wa.png" alt="" /></a>