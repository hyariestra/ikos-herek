<?php include 'component/header.php' ?>


<?php
include 'koneksi.php';
$data = mysqli_query($koneksi,"SELECT * FROM destinasi ");

  ?>
<style>
  h2{
    font-family: 'VAGRoundedLTPro-Black';
    margin-top: 1px !important;
  }
</style>
  <main id="mainContent" class="main">


    <section class="standard-page">
      <div class="wingspan">
        <!-- Standard article wrapper-->
        <article class="article standard-article">
          <!-- Article Header-->
          <header class="article__header">
            <h1 class="setFont" id="p_lt_ctl02_pageplaceholder_p_lt_ctl01_TauziaPopPageTitle_title">Destinasi</h1>
          </header>
          <!-- End: Article Header-->
          <!-- Article Body-->
          <section class="article__body">


            <section class="discover">

              <div class="discover-places">
                <div class="discover-places__wrapper">
                  <div class="discover-places__list">    

                   <?php
                   $no = 1;
                   while($e = mysqli_fetch_array($data)){
                    ?>


                    <div class="discover-places__item" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide00"> <h2><?php echo $e['judul'] ?></h2> <div class="image">
                     <img   src="image/destinasi/<?php echo $e['gambar'] ?>" alt="Jakarta" -="" the="" old="" town=""> 
                   </div> 
                   <p><?php echo $e['keterangan'] ?></p> 
                 </div>
                 <?php

               }
               ?>

          

           </div>
         </div>
       </div>
     </section>



   </section>
 </article>
 <!-- End: Standard article wrapper-->
</div>
</section>

</main>



<?php include 'component/footer.php' ?>